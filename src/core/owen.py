##----------------------------------#
#--- Pour le noyau isentropique ---#
#----------------------------------#

import numpy as np
import math
from termcolor import colored
import warnings

#############################################################################
#                           DEFINITION DES FONCTIONS                        #
#############################################################################

#--- Vitesse axiale au col
def FNVAX(GRK,GP1,Z,R2):
    return GRK*Z+GP1*R2+GP1*GP1*R2*R2/2

#--- Vitesse radiale au col
def FNVRA(GRK,GP1,Z,R2,R):
    return GP1*Z*R2*2/R+GP1*GP1*R2*GRK*R/2*(GRK*Z+.5)

#--- Parametre q+ en fonction du Mach M
def FNPQP(GM1I,M):
    return M/np.sqrt(2*GM1I+M*M)

#--- Distribution du Mach sur l'axe
def FNMACAX(C0,C1,Z2):
    return C0*(C1-np.exp(Z2*Z2))

#--- Angle de Mach alpha
def FNALPHA(MI):
    return np.arcsin(MI)

#--- Equation de la premiere ligne de courant
def FNEFS(CS,RIE,BME,Z):
    return CS*(1+(RIE-1)/BME*Z)

#--- Mach M en fonction du parametre q+ (fonction inverse de FNPQP)
def FNMAC(GM1I,PQ):
    return np.sqrt(2*GM1I*PQ/(1-PQ))

#############################################################################
#                   Calculer le noyau isentropique                          #
#############################################################################

def owen(PARAMS,CST):

    if(PARAMS.VERBOSITY):
        print('\n-----------------------------------')
        print('   Compute the isentropic kernel   ')
        print('-----------------------------------')
    DINT=0
    RCT=PARAMS.RCT
    NIIC=PARAMS.NIIC
    ME=PARAMS.ME
    PTB=PARAMS.PTB
    CS=PARAMS.CS
    EPS=PARAMS.EPS
    BME=PARAMS.BME
    CVGC=PARAMS.CVGC
    CVGL=PARAMS.CVGL
    GAM=CST.GAM

    #--- Calcul des differents coefficients en fonction de gammas
    GP1=GAM+1
    GM1=GAM-1
    GPSM=GP1/GM1
    GMSP=GM1/GP1
    GM1I=1/GM1
    GSM1=GAM/GM1

    #-----  Dimensionnement des vecteurs initial VI et final VF
    #-----  des tableaux axe PC0 et 1ere ligne de courant PC1
    VI=np.zeros([100000,9])
    VF=np.zeros([100000,9])
    PC0=np.zeros([100000,10])
    PC1=np.zeros([100000,9])

    SAVE_Z=np.zeros([100000])
    SAVE_R=np.zeros([100000])
    SAVE_THETA=np.zeros([100000])
    SAVE_MACH=np.zeros([100000])

    #--- Operation unitaire 1
    #--- Calcul de grand gamma, eq. 31 p. 13

    A=1/4-1/16/RCT
    B=2-3/4/RCT
    D=-8/RCT
    B=B/A/3
    P=-1/3/A/A
    Q=B*(P+B*B)-D/A
    P=P/3
    Q=Q/2
    PQ=np.sqrt(Q*Q+P*P*P)
    X=(Q+PQ)**(1/3)+(Q-PQ)**(1/3)
    GGA=X-B
    GRK=np.sqrt(GGA/GP1)
    ZT=GP1*GRK
    ZT=-ZT/2/(4+GRK*ZT)
    if(PARAMS.VERBOSITY):
        print("K = ","{:.3f}".format(GRK))
        
    #--- Operation unitaire 2
    #--- Calcul du debit au col, eq. 62 p. 20

    Z=ZT
    IC=0
    VM1=0
    KK=GRK*GRK/4
    DR=1/NIIC

    for N in range(1,NIIC+1):
        RC=N*DR
        FF7=KK*RC*RC
        W=FNVAX(GRK,GP1,ZT,FF7)
        V=FNVRA(GRK,GP1,ZT,FF7,RC)
        WP1=W+1
        V1=WP1*RC
        V2=1-GMSP*(WP1*WP1+V*V)
        V2=V2**GM1I
        IC=IC+V1*V2+(VM1-V1*V2)/2
        VM1=V1*V2

    IC=IC*DR
    IC=IC*np.sqrt(GMSP)
    if(PARAMS.VERBOSITY):
        print("throat flow = ","{:.3f}".format(IC))

    #--- Operation unitaire 3
    #--- Calcul du rayon de sortie bidimensionnel, eq. 58 p. 19 et la longueur de tuyere, eq ???
    Q=FNPQP(GM1I,ME)
    if(PARAMS.VERBOSITY):
        print("q exit = ","{:.3f}".format(Q))
    RIE=np.sqrt(2.*IC/Q/(1.-Q*Q)**GM1I)
    if(PARAMS.VERBOSITY):
        print("radius of exit = ","{:.3f}".format(RIE))
    ALP=FNALPHA(1./ME)
    TA=np.tan(ALP)
    if(PARAMS.VERBOSITY):
        print("Mach angle = ","{:.3f}".format(ALP))
    ZIE=PTB+RIE/TA
    if(PARAMS.VERBOSITY):
        print("nozzle length = ","{:.3f}".format(ZIE))
    NPCM=int(1./CS)
    if(PARAMS.VERBOSITY):
        print("number of points on Mach cone : ",NPCM)

    #--- Operation unitaire 4
    #--- Calcul des coefficients C0, C1, C2, eqs. 37, 38 p. 14, 41 p. 15
    EE=1+GRK*EPS
    KK=np.sqrt(1-GMSP*EE*EE)
    SS=np.sqrt(2/GP1)
    SK=SS/KK
    K3=SK*EE
    K2=SK*GRK/KK/KK
    if(PARAMS.VERBOSITY):
        print("k2 = ","{:.3f}".format(K2),"k3 = ","{:.3f}".format(K3))

    KK=2*(K3-ME)/K2/BME
    X=-1./KK
    X=X-np.sqrt(X)
    Y=np.exp(X)
    Y1=1/(1+X*KK)
    while (Y-Y1)>CVGC:
        X=(1-Y)/Y/KK
        Y=np.exp(X)
        Y1=1./(1+X*KK)

    if(X<=0):
        print(colored("Calculation impossible, generally the Mach number must be between 2 and 5; or the curvature radius at throat is too large", "red"))
        return exit

    C2=BME/np.sqrt(X)
    C0=K2*BME/2/X/np.exp(X)
    C1=1+ME/C0
    if(PARAMS.VERBOSITY):
        print("C0 = ","{:.3f}".format(C0)," C1 = ","{:.3f}".format(C1)," C2 = ","{:.3f}".format(C2))

    #--- Operation unitaire 6
    #--- Calcul des points du cône de Mach
    DR01=CS*RIE
    DZ01=DR01/TA

    VI[:NPCM+1,0]=PTB+np.linspace(0,NPCM,NPCM+1)*DZ01
    VI[:NPCM+1,1]=np.linspace(0,NPCM,NPCM+1)*DR01
    VI[:,2]=FNPQP(GM1I,ME)
    VI[:,3]=0
    VI[:,4]=ME
    VI[:,5]=FNALPHA(1./ME)
    VI[:,6]=np.sqrt(ME*ME-1)/VI[0,2]
    VI[:,7]=0
    VI[:,8]=0

    Z1=ZIE
    R1=RIE
    Z3=ZIE
    R3=RIE
    THETA3=VI[1,3]
    MACH3=ME

    #--- sauvergarder dans le tableau
    SAVE_Z[0]=Z3
    SAVE_R[0]=R3
    SAVE_THETA[0]=THETA3
    SAVE_MACH[0]=MACH3

    #--- Operation unitaire 9
    #--- Calcul de la 1ere ligne de courant

    PC0[0,:9]=VI[0,:]
    PC0[0,9]=2*DZ01
    I=-1

    while Z1>EPS:
        I=I+1

        #--- Operation unitaire 8
        #--- Parametres de position pour le calcul du point suivant
        Z2=PC0[I,0]
        T2=np.tan(PC0[I,5])
        Z2T=Z2*T2
        Z1=PC0[I,0]-PC0[I,9]

        if Z1>=0 :

            RC1=0

            #--- Operation unitaire 5
            #-- Determination du pas de position suivant pour la premiere ligne de courant, eq. 48 p. 17
            while abs(R3-RC1)>CVGL:
                for N in range(100):
                    ZZ=(PTB-Z1)/C2
                    MA=FNMACAX(C0,C1,ZZ)
                    if(1./MA>1):
                        print(colored("Nozzle length parameter too long or step too small", "red"))
                        return exit
                    A1=FNALPHA(1./MA)
                    T1=np.tan(A1)
                    Z3=(Z1*T1+Z2T)/(T1+T2)
                    R3=(Z2-Z3)*T2
                    RC1=FNEFS(CS,RIE,BME,Z3)
                    Z1=Z1+(R3-RC1)/T1

            Z=Z1
            I1=I+1
            I=I1
            #---repeter l'operation 7 avec I+1
            PC0[I,0]=Z
            PC0[I,1]=0
            ZZ=(PTB-Z)/C2
            PC0[I,4]=FNMACAX(C0,C1,ZZ)
            PC0[I,2]=FNPQP(GM1I,PC0[I,4])
            PC0[I,3]=0
            PC0[I,7]=0
            PC0[I,8]=0
            PC0[I,5]=FNALPHA(1/PC0[I,4])
            PC0[I,6]=np.sqrt(PC0[I,4]*PC0[I,4]-1)/PC0[I,2]
            PC0[I,9]=PC0[I-1,0]-Z                                                   #DZ = Z2 - Z
            #--- Calcul de Z3,R3,M3,Theta3,M3,alpha3,Q3
            I=I1-1
            PC1[I1,0]=Z3
            PC1[I1,1]=R3
            PC1[I1,2]=(PC0[I,2]*PC0[I,6]+PC0[I1,2]*PC0[I1,6])/(PC0[I,6]+PC0[I1,6])  # q3
            PC1[I1,3]=-PC0[I,6]*(PC1[I1,2]-PC0[I,2])/2                              # theta3
            QP2=PC1[I1,2]*PC1[I1,2]
            PC1[I1,4]=FNMAC(GM1I,QP2)                                               # M3
            PC1[I1,5]=FNALPHA(1./PC1[I1,4])                                         # alpha3
            PC1[I1,6]=np.sqrt(PC1[I1,4]*PC1[I1,4]-1)/PC1[I1,2]                      # Q3
            TH=PC1[I1,3]
            ST=np.sin(TH)
            AL=PC1[I1,5]
            SA=np.sin(AL)
            PC1[I1,7]=ST*SA/np.sin(TH+AL)
            PC1[I1,8]=ST*SA/np.sin(TH-AL)
        #---

    #-- Stockage du profil isentropique dans PC0
    #-- Stockage de la premiere ligne de courant dans PC1

    #--- Operation unitaire 11
    #--- Calcul de l'ensemble des caracteristiques

    IS=I
    for N in range(IS+1):
        N1=N+1
        VF[1,:]=PC1[N1,:]

        #--- operation 10
        #--- Calcul des parametres d une ligne quelconque  pour le calcul du profil
        with warnings.catch_warnings():
            warnings.filterwarnings('error')
            try:
                PC0[1,8]=PC1[N1,2]*(1-PC1[N1,2]*PC1[N1,2])**GM1I*PC1[N1,1]*PC1[N1,1]*np.sin(PC1[N1,5])/np.sin(PC1[N1,5]+PC1[N1,3])/2
            except:
                print(colored("Nozzle length parameter too small", "red"))
                return exit
        for NJ in range(1,NPCM+1):
            NN=NJ+1
            TH2=VI[NJ,3]
            TH1=VF[NJ,3]
            AL2=VI[NJ,5]
            AL1=VF[NJ,5]
            Z2=VI[NJ,0]
            Z1=VF[NJ,0]
            R2=VI[NJ,1]
            R1=VF[NJ,1]
            T2=np.tan(TH2-AL2)
            T1=np.tan(TH1+AL1)
            Z3=(R2-R1-Z2*T2+Z1*T1)/(T1-T2)
            R3=R2+(Z3-Z2)*T2
            PQP2=VI[NJ,2]
            PQP1=VF[NJ,2]
            Q2=VI[NJ,6]
            Q1=VF[NJ,6]
            G2=VI[NJ,8]
            F1=VF[NJ,7]
            PQP3=PQP1*Q1+PQP2*Q2+F1*(R3-R1)/R1+G2*(R3-R2)/R2+TH2-TH1
            PQP3=PQP3/(Q1+Q2)
            TH3=TH2-Q2*(PQP3-PQP2)+G2*(R3-R2)/R2
            QP2=PQP3*PQP3
            VF[NN,0]=Z3
            VF[NN,1]=R3
            VF[NN,2]=PQP3
            VF[NN,3]=TH3
            M3=FNMAC(GM1I,QP2)
            AL3=FNALPHA(1./M3)
            Q3=np.sqrt(M3*M3-1)/PQP3
            VF[NN,4]=M3
            VF[NN,5]=AL3
            VF[NN,6]=Q3
            ST=np.sin(TH3)
            SA=np.sin(AL3)
            VF[NN,7]=ST*SA/np.sin(TH3+AL3)
            VF[NN,8]=SA*ST/np.sin(TH3-AL3)
            DE1=PQP1*(1-PQP1*PQP1)**GM1I*R1*np.sin(AL1)/np.sin(AL1+TH1)
            DE3=PQP3*(1-QP2)**GM1I*R3*SA/np.sin(AL3+TH3)
            DE=(DE1+DE3)/2*(R3-R1)
            PC0[NN,8]=PC0[NJ,8]+DE

        NJ=np.where(PC0[:,8]-IC>0)[0][0]-1

        PC0[N1,1]=VF[NJ,1]+(IC-PC0[NJ,8])*(VF[NJ+1,1]-VF[NJ,1])/(PC0[NJ+1,8]-PC0[NJ,8])
        PC0[N1,3]=VF[NJ,0]+(PC0[N1,1]-VF[NJ,1])*(VF[NJ+1,0]-VF[NJ,0])/(VF[NJ+1,1]-VF[NJ,1])
        PC0[N1,7]=VF[NJ,4]+(PC0[N1,1]-VF[NJ,1])*(VF[NJ+1,4]-VF[NJ,4])/(VF[NJ+1,1]-VF[NJ,1])
        THETINT=VF[NJ,3]+(PC0[N1,1]-VF[NJ,1])*(VF[NJ+1,3]-VF[NJ,3])/(VF[NJ+1,1]-VF[NJ,1])
        NPCM=NJ+1
        #---

        VI[1:NPCM+2,:]=VF[1:NPCM+2,:]

        SAVE_Z[N+1]=PC0[N1,3]
        SAVE_R[N+1]=PC0[N1,1]
        SAVE_THETA[N+1]=THETINT
        SAVE_MACH[N+1]=PC0[N1,7]

    #--- Operation unitaire 12
    #--- Calcul des caracteristiques en z=EPS

    VI[0,1]=.0000001
    VI[1:NPCM+2,:]=VF[1:NPCM+2,:]
    Z=EPS
    KK=GRK*GRK/4
    JD=0

    I=1
    while(True):
        JD=JD+2
        RC=VI[JD,1]
        if RC>=1:
            if(PARAMS.VERBOSITY):
                print("End of the calculation of the isentropic core !\n")
            break

        FF7=KK*RC*RC
        W=FNVAX(GRK,GP1,EPS,FF7)
        V=FNVRA(GRK,GP1,EPS,FF7,RC)
        WP1=W+1
        VF[JD,0]=EPS
        VF[JD,1]=RC
        VF[JD,2]=np.sqrt(GMSP*(WP1*WP1+V*V))
        QP2=VF[JD,2]*VF[JD,2]
        VF[JD,3]=np.arctan(V/WP1)
        VF[JD,4]=FNMAC(GM1I,QP2)
        VF[JD,5]=FNALPHA(1/VF[JD,4])
        VF[JD,6]=1/VF[JD,2]/np.tan(VF[JD,5])
        VF[JD,7]=np.sin(VF[JD,3])*np.sin(VF[JD,5])/np.sin(VF[JD,3]+VF[JD,5])
        VF[JD,8]=np.sin(VF[JD,3])*np.sin(VF[JD,5])/np.sin(VF[JD,3]-VF[JD,5])

        #--- operation 2
        #--- Calcul du debit au col; eq. 62 p. 20
        Z=EPS
        DC=0
        KK=GRK*GRK/4
        DR=(VF[JD,1]-VI[JD-2,1])/NIIC

        for N in range(1,NIIC):
            RC=N*DR+VI[JD-2,1]
            FF7=KK*RC*RC
            W=FNVAX(GRK,GP1,EPS,FF7)
            V=FNVRA(GRK,GP1,EPS,FF7,RC)
            WP1=W+1
            V1=WP1*RC
            V2=1-GMSP*(WP1*WP1+V*V)
            V2=V2**GM1I
            DC=DC+V1*V2

        FF7=KK*VI[JD-2,1]*VI[JD-2,1]
        W=FNVAX(GRK,GP1,EPS,FF7)
        V=FNVRA(GRK,GP1,EPS,FF7,VI[JD-2,1])
        WP1=1+W
        V1=WP1*VI[JD-2,1]
        V2=1-GMSP*(WP1*WP1+V*V)
        V2=V2**GM1I
        DC=DC+V2/2*V1
        FF7=KK*VF[JD,1]*VF[JD,1]
        W=FNVAX(GRK,GP1,EPS,FF7)
        V=FNVRA(GRK,GP1,EPS,FF7,VF[JD,1])
        WP1=W+1
        V1=WP1*VF[JD,1]
        V2=1-GMSP*(WP1*WP1+V*V)
        V2=V2**GM1I
        DC=DC+V2*V1/2
        DC=DC*DR
        DC=DC*np.sqrt(GMSP)
        #---

        DINT=DINT+DC
        #--- operation 10
        #--- Calcul des parametres d une ligne quelconque
        PC0[JD,8]=DINT
        for NJ in range(JD,NPCM+1):
            NN=NJ+1
            TH2=VI[NJ,3]
            TH1=VF[NJ,3]
            AL2=VI[NJ,5]
            AL1=VF[NJ,5]
            T2=np.tan(TH2-AL2)
            T1=np.tan(TH1+AL1)
            Z2=VI[NJ,0]
            Z1=VF[NJ,0]
            R2=VI[NJ,1]
            R1=VF[NJ,1]
            Z3=(R2-R1-Z2*T2+Z1*T1)/(T1-T2)
            R3=R2+(Z3-Z2)*T2
            PQP2=VI[NJ,2]
            PQP1=VF[NJ,2]
            Q2=VI[NJ,6]
            Q1=VF[NJ,6]
            G2=VI[NJ,8]
            F1=VF[NJ,7]
            PQP3=PQP1*Q1+PQP2*Q2+F1*(R3-R1)/R1+G2*(R3-R2)/R2+TH2-TH1
            PQP3=PQP3/(Q1+Q2)
            TH3=TH2-Q2*(PQP3-PQP2)+G2*(R3-R2)/R2
            QP2=PQP3*PQP3
            VF[NN,0]=Z3
            VF[NN,1]=R3
            VF[NN,2]=PQP3
            VF[NN,3]=TH3
            M3=FNMAC(GM1I,QP2)
            AL3=FNALPHA(1./M3)
            Q3=np.sqrt(M3*M3-1)/PQP3
            VF[NN,4]=M3
            VF[NN,5]=AL3
            VF[NN,6]=Q3
            ST=np.sin(TH3)
            SA=np.sin(AL3)
            VF[NN,7]=ST*SA/np.sin(TH3+AL3)
            VF[NN,8]=SA*ST/np.sin(TH3-AL3)
            DE1=PQP1*(1-PQP1*PQP1)**GM1I*R1*np.sin(AL1)/np.sin(AL1+TH1)
            DE3=PQP3*(1-QP2)**GM1I*R3*SA/np.sin(AL3+TH3)
            DE=(DE1+DE3)/2*(R3-R1)
            PC0[NN,8]=PC0[NJ,8]+DE

        NJ=np.where(PC0[:,8]-IC>0)[0][0]-1

        PC0[N1,1]=VF[NJ,1]+(IC-PC0[NJ,8])*(VF[NJ+1,1]-VF[NJ,1])/(PC0[NJ+1,8]-PC0[NJ,8])
        PC0[N1,3]=VF[NJ,0]+(PC0[N1,1]-VF[NJ,1])*(VF[NJ+1,0]-VF[NJ,0])/(VF[NJ+1,1]-VF[NJ,1])
        PC0[N1,7]=VF[NJ,4]+(PC0[N1,1]-VF[NJ,1])*(VF[NJ+1,4]-VF[NJ,4])/(VF[NJ+1,1]-VF[NJ,1])
        THETINT=VF[NJ,3]+(PC0[N1,1]-VF[NJ,1])*(VF[NJ+1,3]-VF[NJ,3])/(VF[NJ+1,1]-VF[NJ,1])
        NPCM=NJ+1

        Z3=PC0[N1,3]
        R3=PC0[N1,1]
        THETA3=THETINT
        MACH3=PC0[N1,7]

        SAVE_Z[N1+I]=Z3
        SAVE_R[N1+I]=R3
        SAVE_THETA[N1+I]=THETA3
        SAVE_MACH[N1+I]=MACH3
        I+=1
        #---
        VI[JD:NPCM+2,:]=VF[JD:NPCM+2,:]

    # Couper pour enlever d'eventuelles valeurs negatives et retourner le profil de la tuyere
    NPTS=np.where(SAVE_Z==0)[0][0]
    SAVE_Z=np.flip(SAVE_Z[:NPTS], axis=0)
    SAVE_R=np.flip(SAVE_R[:NPTS], axis=0)
    SAVE_THETA=np.flip(SAVE_THETA[:NPTS], axis=0)
    SAVE_MACH=np.flip(SAVE_MACH[:NPTS], axis=0)

    # recalibrer Z,R pour que R[0] coincide à ce stade avec 1 exactement de manière a obtenir exactement R[0]=RCOL+EDC à la fin
    SAVE_Z=SAVE_Z/SAVE_R[0]
    SAVE_R=SAVE_R/SAVE_R[0]

    # Mise a l'echelle de la tuyere
    SAVE_Z=SAVE_Z*PARAMS.RCOL
    SAVE_R=SAVE_R*PARAMS.RCOL

    if(PARAMS.VERBOSITY):

        #--- Calcul du debit massique et du debit vol.
        TC=PARAMS.T0*2/GP1
        PC=PARAMS.P0*(2/GP1)**GSM1
        RHOC=PC/TC*CST.M*CST.AVO/CST.R_CST_UNIV
        UC=(CST.GAM*CST.BOLTZ*TC/CST.M)**0.5
        Q=RHOC*UC*np.pi*PARAMS.RCOL**2

        print("Debit = ","{:.3f}".format(Q*1e3)," g/s = ","{:.3f}".format(Q/CST.M/CST.AVO*CST.Vm_CNTP*1e3*60)," l/mn")
        print("Nozzle length = ","{:.3f}".format(SAVE_Z[-1]*100)," cm")
        print("Isentropic radius at exit = ","{:.3f}".format(SAVE_R[-1]*100)," cm")

    return SAVE_Z,SAVE_R,SAVE_THETA,SAVE_MACH

#############################################################################
#                   Fin du calcul du noyau isentropique                     #
#############################################################################
