class PARAMETERS:

    NOMTUYERE='M4_200mbar_N2_test2.dat' # Save nozzle profile file

    ME  = 4         # Mach number
    GAZ = 'He'      # "He", "Ar", "N2" # Gas type
    EPS = 1e-002    # Calculation step
    PTB = 16        # Length parameter (integer)
    T   = 50        # Flow temperature

    RCOL = 1.       # Radius at throat (cm)
    P0   = 200.     # Reservoir pressure (mbar)
    T0   = 293.     # Reservoir temperature (K)
    TP   = 293.     # Wall temperature (K)
    EDC  = 1e-004   # Displacement thickness at throat (cm)

    RCT=5           # Radius of curvature at throat
    NPC=9001        # Number of points for the convergent
    NIIC=100
    CS=EPS
    BME=PTB-EPS
    CVGL=1e-006     # Convergence criteria
    CVGC=CVGL
    
    VERBOSITY=False
    BOUNDARYLAYER=True
    PLOT=True


    #-- Unit homogenization in IS
    P0=P0*100       # Reservoir pressure: mbar -> Pa
    RCOL=RCOL/100   # Radius at throat : cm -> m
