#--------------------------------#
#--- Pour les couches limites ---#
#--------------------------------#

import numpy as np

#--- Loi de viscosite RENNES
def FNMU(GAZ,MUREF,TREF,T):
    if GAZ == "He":
        return MUREF*(T/TREF)**0.647
    elif GAZ == "Ar":
        return MUREF*(T/TREF)**0.93
    elif GAZ == "N2":
        return MUREF*(T/TREF)**0.5*(1+114/TREF)/(1+114/T)

#--- Loi de temperature de l'ecoulement
def FNTE(T0,GM1,MACH):
    return T0/(1+GM1/2*MACH**2)

#--- Temperature de frottement dans la couche limite
def FNTF(RT,GM1,MACH,T):
    return T*(1+RT*GM1/2*MACH**2)

#--- Parametre de forme
def FNH(HI,ALPHA,BETA,TP,MACH,T1,T2):
    return HI+ALPHA*MACH**2+BETA*(TP-T2)/T1

#--- Loi de temperature de Monaghan
def FNTS(TP,T1,T2):
    return T1+0.54*(TP-T1)+0.16*(T2-T1)

#--- Loi de densite de l'ecoulement
def FNRHO(GM1I,RHO0,T0,T):
    return RHO0*(T/T0)**GM1I

#--- Loi de vitesse de l'ecoulement
def FNU(GAM,BOLTZ,M,MACH,T):
    return MACH*(GAM*BOLTZ*T/M)**0.5

#--- résolution du problème
def michel(Z,R,MACH,PARAMS,CST):

    #--- Calcul des differents coefficients en fonction de gammas
    GP1=CST.GAM+1
    GM1=CST.GAM-1
    GPSM=GP1/GM1
    GMSP=GM1/GP1
    GM1I=1/GM1
    GSM1=CST.GAM/GM1

    NPTS=len(Z)

    UE=np.zeros([NPTS])       # vitesse exterieur
    DMAC=np.zeros([NPTS])     # derivee du nombre de Mach du noyau isentropique
    DUE=np.zeros([NPTS])      # derivee de la vitesse du noyau isentropique
    F=np.zeros([NPTS])        # fonction phi
    E=np.zeros([NPTS])        # fonction E
    INE=np.zeros([NPTS])      # partie integrale de la fonction E
    IN=np.zeros([NPTS])       # partie integrale de la solution de Michel
    DELTA=np.zeros([NPTS])    #
    DELTA1=np.zeros([NPTS])   # epaisseur de deplacement du profil
    DELTA2=np.zeros([NPTS])   # epaisseur de deplacement de la quantite de mouvement
    RAPTEMP=np.zeros([NPTS])  # rapport de temperatures
    H=np.zeros([NPTS])        # parametre de forme
    MU=np.zeros([NPTS])       # viscosite dynamique du gaz
    G=np.zeros([NPTS])        # facteur de compressibilite
    RHO=np.zeros([NPTS])      # densite du noyau isentropique

    DELTA1[0] = PARAMS.EDC

    #--- Calcul de la densite de l'ecoulement
    RHO0=PARAMS.P0/PARAMS.T0*CST.M*CST.AVO/CST.R_CST_UNIV

    #--- Calcul de quantites necessaires à la solution integrale de Michel
    for I in range(NPTS):
        TE=FNTE(PARAMS.T0,GM1,MACH[I])         # temperature de l'ecoulement exterieur
        TF=FNTF(CST.RT,GM1,MACH[I],TE)      # temperature de frottement
        TS=FNTS(PARAMS.TP,TE,TF)          # temperature de Monaghan
        MUS=FNMU(PARAMS.GAZ,CST.MUREF,CST.TREF,TS)            # viscosite à la temperature de Monaghan
        MU[I]=FNMU(PARAMS.GAZ,CST.MUREF,CST.TREF,TE)          # viscosite de l'ecoulement exterieur
        H[I]=FNH(CST.HI,CST.ALPHA,CST.BETA,PARAMS.TP,MACH[I],TE,TF)  # parametre de forme
        G[I]=TE/TS*MUS/MU[I]    # facteur de compressibilite
        RHO[I]=FNRHO(GM1I,RHO0,PARAMS.T0,TE)        # densite de l'ecoulement exterieur
        UE[I]=FNU(CST.GAM,CST.BOLTZ,CST.M,MACH[I],TE)    # vitesse de l'ecoulement exterieur
        F[I]=(UE[I]**(CST.HI+2)*RHO[I]**(1-CST.ALPHA))**(CST.M0+1)  # fonction phi
        # pour la fonction E
        RAPTEMP[I]=(PARAMS.TP-TF)/TE#*TE/T0 # note TE ~ T0
        if I != NPTS-1:           # derivee du nombre de Mach à l'ordre 1 pour ensuite calculer la derivee de la vitesse de l'ecoulement exterieur
            DMAC[I]=(MACH[I+1]-MACH[I])/(Z[I+1]-Z[I])
        else:
            DMAC[I]=0

    # --- Calcul de la fonction E
    for I in range(1,NPTS):   # integration de l'integrale de la fonction E
        INE[I]=INE[I-1]+.5*(Z[I]-Z[I-1])*(RAPTEMP[I-1]*DMAC[I-1]/MACH[I-1]+RAPTEMP[I]*DMAC[I]/MACH[I])
    E=np.exp(CST.BETA*(CST.M0+1)*INE)     # fonction E

    #-- Calcul de l'integrale de la solution de Michel
    for I in range(1,NPTS):
        F1=G[I-1]*F[I-1]*E[I-1]*R[I-1]**2*MU[I-1]/RHO[I-1]/UE[I-1]
        F2=G[I]*F[I]*E[I]*R[I]**2*MU[I]/RHO[I]/UE[I]
        IN[I]=IN[I-1]+0.5*(Z[I]-Z[I-1])*(F1+F2)

    B0=F[0]*E[0]*(DELTA1[0]*R[0]/H[0])**(CST.M0+1)
    #-- Et finalement DELTA2, DELTA1, DELTA suivant la formule de Michel
    DELTA2[1:]=((B0+CST.K*(CST.M0+1)*IN[1:])/F[1:]/E[1:])**(1/(CST.M0+1))/R[1:]
    DELTA1[1:]=H[1:]*DELTA2[1:]
    DELTA[1:]=DELTA1[1:]*(1+2.5*CST.HI/H[1:])


    if(PARAMS.VERBOSITY):
        print('\n-----------------------------------')
        print('    Compute the boundary layers    ')
        print('-----------------------------------')

        print("Boundary layer thickness at exit = ","{:.3f}".format(DELTA[-1]*100), "cm")
        print("delta/r = ","{:.3f}".format(DELTA[-1]/R[-1]*100), " %")
        print("End of the calculation of the boundary layers !\n")


    return Z,R,DELTA,DELTA1
