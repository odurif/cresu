class CONSTANTS:

    def __init__(self,GAZ):

        self.R_CST_UNIV=8.31446261815324
        self.AVO=6.02214076E+23
        self.BOLTZ=1.38E-23
        self.Vm_CNTP=22.414e-3 # molar volume under standard temperature (0°C) and pressure (1 atm) conditions

        if GAZ == "He":
            self.GAM=5./3           # Heat capacity ratio
            self.M=4.0026           # Molar mass (g/mol)
            self.PRAN=0.66          # Prandlt number = mu*cp/cond. Therm.
            self.MUREF=1.887E-05    # Viscosity at temperature Tref (Pa.s)
            self.TREF=273
            self.CP=5193            # Specific heat in J/kg/K
        elif GAZ == "Ar":
            self.GAM=5./3
            self.M=39.948
            self.PRAN=0.66
            self.MUREF=2.116E-05
            self.TREF=273
            self.CP=519.3
        elif GAZ == "N2":
            self.GAM=7./5
            self.M=28.013
            self.PRAN=.73
            self.MUREF=5.33e-6       # Marquette value 5.48E-06
            self.TREF=80.6           # Marquette value 78.6
            self.CP=1033

        #-- Unit homogenization in IS
        self.M=self.M*1e-3/self.AVO     # atomic mass of carrier gas : g/mol -> kg

        #--- Michel's expression for boundary layers
        self.RT=self.PRAN**0.5 # Laminar parietal heat factor
        self.ALPHA=self.GAM*self.BOLTZ/self.CP/self.M*2.05336*self.PRAN**0.63751 # Shape parameter coefficient
        self.BETA=-0.71107*self.PRAN+3.41551
        self.HI=2.591 #  incompressible shape parameter
        self.M0=1
        self.K=.2205 # constant "b" in Michel's solution
