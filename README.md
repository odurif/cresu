# Uniform Supersonic Flow

**This repository is dedicated to a program to compute de Laval nozzles profiles in rarefied gas flow.**

The isentropic kernel of a supersonic nozzle is computed using the method of characteristics following the Owen's work ("An improved method of supersonic nozzle design for rarefied gas flows”, University of California, 1950). A laminar boundary layer can be added according to Michel's integral method (“Aérodynamique: Couches limites, frottement et transfert de chaleur”, ENSAE, 1963).

The code is explained in: \
O.Durif, Design of de Laval nozzles for gas-phase molecular studies in uniform supersonic flow. \
*Physics of Fluids* 34, 013605 (2022). \
[DOI: 10.1063/5.0060362](https://doi.org/10.1063/5.0060362)

## Run online

[https://odurif.gitlab.io/cresu/](https://odurif.gitlab.io/cresu/voici/render/usf.html?)


## Run on local

```console
pip install -r requirements.txt
```

```console
voila src/usf.ipynb
```

## Author   
O. Durif <olivier@durif.fr>
